﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    public float turnSpeed = 20f;
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    private int count;


    public bool invulnerable;
    float invulnerableCooldown;
    public Material[] NewMaterialRefs;
    public SkinnedMeshRenderer rend;
    public GameObject[] doors;
    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        count = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");


        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        // setting direction for movement.
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }
    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    private void Update()
    {
        if (invulnerable)
        {
            if (rend != null)
            {
              
                Material[] mats = new Material[] { NewMaterialRefs[0], NewMaterialRefs[1] }; // WORKS
                rend.materials = mats;

                //rend.materials[0] = NewMaterialRefs[0];
               // rend.materials[1] = NewMaterialRefs[1];
            }
            invulnerableCooldown -= Time.deltaTime;
            print("invulnerabilityCooldown:" + invulnerableCooldown);
            if (invulnerableCooldown <= 0f)
            {
                invulnerable = false;
                Material[] mats = new Material[] { NewMaterialRefs[2], NewMaterialRefs[3] };
                rend.materials = mats;

            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && invulnerableCooldown <= 0f)
        {
            invulnerable = true;
            invulnerableCooldown = 5f;


        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            print(count);
            if (count == 3)
            {
                foreach (GameObject door in doors)
                {
                    door.SetActive(false);
                }


                //GameObject[] doors = FindObjects

                //doors.gameObject.SetActive(false);


                
            }
        }
        
        

    }
}