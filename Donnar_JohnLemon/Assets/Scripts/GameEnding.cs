﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public Text Timer;
    public PlayerMovement invisRef;
   

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;

    public float timeTaken;
    public float timeHighScore;


    private void Start()
    {
        timeHighScore = PlayerPrefs.GetFloat("timeHighScore", 999999999f); //loads current best time, sets bar for high scores.
        print("Current fastest time:" + timeHighScore); 
        
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;

            print("You took this long to finish:" + timeTaken);
            if (timeTaken < timeHighScore)
            {
                timeHighScore = timeTaken; //Save new fastest time
                print("New fastest time:" + timeHighScore); //Replace with ui display
                PlayerPrefs.SetFloat("timeHighscore", timeHighScore);// save new best time
            }
        }
    }

    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }

    void Update()
    {
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
        else
        {//If not caught or at end increase timer.

            timeTaken += Time.deltaTime; 
            Timer.text = "Time Taken:" + (int)(timeTaken); 
            print("timeTaken" + timeTaken);
            if (invisRef.invulnerable)
            {
                timeTaken += (Time.deltaTime *5/4);
            }


        }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
